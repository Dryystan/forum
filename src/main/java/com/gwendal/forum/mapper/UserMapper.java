package com.gwendal.forum.mapper;

import java.util.Set;

import org.springframework.stereotype.Component;

import com.gwendal.forum.dto.UserDto;
import com.gwendal.forum.dto.creation.UserCreationDto;
import com.gwendal.forum.models.Role;
import com.gwendal.forum.models.User;

@Component
public class UserMapper {
	public UserDto toDto(User user){
		Long id = user.getId();
        String username = user.getUsername();
        String email = user.getEmail();
        Set<Role> roles = user.getRoles();

        return new UserDto(id, username, email, roles);
    }

    public User toUser(UserCreationDto userDto){
        return new User(userDto.getUsername(), userDto.getEmail(), userDto.getPassword(), userDto.getRoles());
    }
}
