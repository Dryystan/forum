package com.gwendal.forum.dao.impl;

import java.util.Optional;
import javax.persistence.Query;

import com.gwendal.forum.dao.RoleDao;
import com.gwendal.forum.enums.Roles;
import com.gwendal.forum.models.Role;

public class RoleDaoImpl extends GenericDaoImpl<Role, Long> implements RoleDao {

	@SuppressWarnings("unchecked")
	@Override
	public Optional<Role> findByName(Roles name) {
		Query query = entityManager.createQuery(
                "SELECT r "
                + "FROM Role r "
                + "WHERE r.name=:name")
				.setParameter("name", name);
        Role role = null;
        try {
        	role = (Role) query.getSingleResult();
        }
        catch(Exception e) {
        	logger.error("Error finding role with name: " + role);
        }
        return Optional.ofNullable(role);
	}
}
