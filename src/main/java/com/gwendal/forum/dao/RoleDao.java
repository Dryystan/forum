package com.gwendal.forum.dao;

import java.util.Optional;

import com.gwendal.forum.enums.Roles;
import com.gwendal.forum.models.Role;

public interface RoleDao extends GenericDao<Role, Long>{
	public Optional<Role> findByName(Roles name);
}
