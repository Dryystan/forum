package com.gwendal.forum.dao;

import java.util.Optional;

import com.gwendal.forum.models.User;

public interface UserDao extends GenericDao<User, Long> {
	public Optional<User> findByUsername(String username);
	public boolean existsUsername(String username);
	public Optional<User> findByEmail(String email);
	public boolean existsEmail(String email);
	public void deleteById(Long id);
}
