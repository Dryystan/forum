package com.gwendal.forum.config;

import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

import com.gwendal.forum.dao.impl.RoleDaoImpl;
import com.gwendal.forum.security.services.UserDetailsServiceImpl;

@Configuration
@EnableAutoConfiguration
public class ServiceConfig {
	@Bean
	public RoleDaoImpl RoleDaoFactory() {
		return new RoleDaoImpl();
	}
	
	@Bean
	public UserDetailsServiceImpl userDetailsServiceFactory() {
		return new UserDetailsServiceImpl();
	}
}
