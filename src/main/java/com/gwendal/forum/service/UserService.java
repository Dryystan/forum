package com.gwendal.forum.service;

import java.util.Optional;

import com.gwendal.forum.models.User;

public interface UserService extends GenericService<User, Long>{
	public Optional<User> findByUsername(String username);
	public boolean existsUsername(String username);
	public Optional<User> findByEmail(String email);
	public boolean existsEmail(String email);
	public void deleteById(Long id);
}
