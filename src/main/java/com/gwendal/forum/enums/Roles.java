package com.gwendal.forum.enums;

public enum Roles {
	USER,
	MODERATOR,
	ADMIN
}
