package com.gwendal.forum.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gwendal.forum.dto.UserDto;
import com.gwendal.forum.dto.creation.UserCreationDto;
import com.gwendal.forum.mapper.UserMapper;
import com.gwendal.forum.models.User;
import com.gwendal.forum.service.impl.UserServiceImpl;

@RestController
@RequestMapping("users")
@CrossOrigin
public class UserController {
	
	@Autowired
	private UserServiceImpl service;
	
	@Autowired
	private UserMapper mapper;
	
	@GetMapping("")
    public List<UserDto> findAll(){
        return this.service.findAll()
                .stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }

    @GetMapping("{id}")
    public UserDto findById(@PathVariable long id){
        return mapper.toDto(this.service.findById(id));
    }
    
    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
    	this.service.deleteById(id);
    }
}
