package com.gwendal.forum.dto.creation;

import java.util.HashSet;
import java.util.Set;

import com.gwendal.forum.models.Role;

public class UserCreationDto {
	private Long id;
	private String username;
	private String email;
	private String password;
	private Set<Role> roles = new HashSet<>();
	
	/**
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id id à définir
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @return username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username username à définir
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email email à définir
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password password à définir
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return roles
	 */
	public Set<Role> getRoles() {
		return roles;
	}
	/**
	 * @param roles roles à définir
	 */
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
}
